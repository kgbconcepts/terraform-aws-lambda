# lambda
output "name" {
  value = var.lambda_function_name
}

output "arn" {
  value = element(concat(aws_lambda_function.lambda_function.*.arn, [""]), 0)
}

output "qualified_arn" {
  value = element(concat(aws_lambda_function.lambda_function.*.qualified_arn, [""]), 0)
}

output "invoke_arn" {
  value = element(concat(aws_lambda_function.lambda_function.*.invoke_arn, [""]), 0)
}

output "version" {
  value = element(concat(aws_lambda_function.lambda_function.*.version, [""]), 0)
}

output "last_modified" {
  value = element(concat(aws_lambda_function.lambda_function.*.last_modified, [""]), 0)
}

output "source_code_hash" {
  value = element(concat(aws_lambda_function.lambda_function.*.source_code_hash, [""]), 0)
}

output "source_code_size" {
  value = element(concat(aws_lambda_function.lambda_function.*.source_code_size, [""]), 0)
}

# iam
output "role_id" {
  value = element(concat(aws_iam_role.lambda_role.*.id, [""]), 0)
}

output "role_arn" {
  value = element(concat(aws_iam_role.lambda_role.*.arn, [""]), 0)
}

output "role_create_date" {
  value = element(concat(aws_iam_role.lambda_role.*.create_date, [""]), 0)
}

output "role_name" {
  value = element(concat(aws_iam_role.lambda_role.*.name, [""]), 0)
}

output "role_policy_id" {
  value = element(concat(aws_iam_role_policy.lambda_role_policy.*.id, [""]), 0)
}

output "role_policy_name" {
  value = element(concat(aws_iam_role_policy.lambda_role_policy.*.name, [""]), 0)
}

output "role_policy_role" {
  value = element(concat(aws_iam_role_policy.lambda_role_policy.*.role, [""]), 0)
}
