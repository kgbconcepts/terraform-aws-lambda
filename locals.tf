locals {
  accountid               = var.accountid
  assume_role_policy_file = var.assume_role_policy != "" ? var.assume_role_policy : "${path.module}/policy/assume_role_policy.json.tpl"
  role_policy_file        = var.role_policy != "" ? var.role_policy : "${path.module}/policy/role_policy.json.tpl"
}
