resource "aws_lambda_function" "lambda_function" {
  count                          = var.enabled ? 1 : 0
  filename                       = var.package_file
  function_name                  = var.lambda_function_name
  description                    = var.lambda_function_description
  role                           = aws_iam_role.lambda_role.0.arn
  handler                        = var.handler
  source_code_hash               = var.package_file_base64sha256
  kms_key_arn                    = var.kms_key_arn
  runtime                        = var.runtime
  memory_size                    = var.memory_size
  timeout                        = var.timeout
  reserved_concurrent_executions = var.reserved_concurrent_executions
  publish                        = var.publish

  environment {
    variables = var.lambda_environment
  }

  tags = merge(
    var.tags,
    map(
      "Name", var.lambda_function_name
    )
  )

  lifecycle {
    ignore_changes = [
      filename,
      last_modified,
    ]
  }
}
