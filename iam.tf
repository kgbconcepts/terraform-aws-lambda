data "template_file" "lambda_assume_role_policy" {
  count    = var.enabled ? 1 : 0
  template = file(local.assume_role_policy_file)
  vars = {
    v = "1.0"
  }
}

data "template_file" "lambda_role_policy" {
  count    = var.enabled ? 1 : 0
  template = file(local.role_policy_file)
  vars = {
    v = "1.0"
  }
}

# role create if enabled.  policy from var
resource "aws_iam_role" "lambda_role" {
  count              = var.enabled ? 1 : 0
  name               = var.role_name
  assume_role_policy = data.template_file.lambda_assume_role_policy.0.rendered # templatefile(local.assume_role_policy_file, { v = "1.0" })
}

# policy create if enabled. assign to role
# merge policies above
resource "aws_iam_role_policy" "lambda_role_policy" {
  count  = var.enabled ? 1 : 0
  name   = var.role_policy_name
  role   = aws_iam_role.lambda_role.0.id
  policy = data.template_file.lambda_role_policy.0.rendered # templatefile(local.role_policy_file, { v = "1.0" })
}
