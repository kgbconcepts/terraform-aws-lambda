variable "enabled" {
  type        = bool
  description = "Set to `false` to prevent the module from creating any resources"
  default     = true
}

variable "region" {
  type        = string
  description = "Set region"
  default     = "us-east-1"
}

variable "accountid" {
  type        = string
  description = "Set accountid"
  default     = "1111111111"
}

variable "package_file" {
  type        = string
  description = "the function/app package file"
  default     = "example.zip"
}

variable "package_file_base64sha256" {
  type        = string
  description = "the function/app package file base64sha256 hash"
  default     = "1111111111"
}

variable "lambda_function_name" {
  type        = string
  description = "the function/app name for lambda app"
  default     = "example_hello_world"
}

variable "lambda_function_description" {
  type        = string
  description = "the function/app description"
  default     = "this is example hello world"
}

variable "runtime" {
  type        = string
  description = "the function/app runtime"
  default     = "nodejs8.10"
}

variable "handler" {
  type        = string
  description = "the function/app handler"
  default     = "lambda_function.lambda_handler"
}

variable "memory_size" {
  type        = string
  description = "the function/app memory allocation"
  default     = "128"
}

variable "timeout" {
  type        = string
  description = "the function/app the amount of time it can run"
  default     = "3"
}

variable "reserved_concurrent_executions" {
  type        = string
  description = "the function/app the reserved concurrent executions"
  default     = "3"
}

variable "publish" {
  type        = string
  description = "publish function/app lambda"
  default     = false
}

variable "lambda_environment" {
  type        = map(string)
  description = "environment variables for lambda function/app"
  default = {
    foo = "bar"
  }
}

variable "tags" {
  type        = map(string)
  description = "tags to be passed down"
  default = {
    service = "lambda"
  }
}

variable "role_name" {
  type        = string
  description = "lambda function role name"
  default     = "LambdaRoleforsecretMaker"
}

variable "role_policy_name" {
  type        = string
  description = "lambda function role policy name"
  default     = "LambdaRolePolicyforsecretMaker"
}

variable "kms_key_arn" {
  type        = string
  description = "ARN of the KMS key used for decrypting"
  default     = ""
}

variable "assume_role_policy" {
  type        = string
  description = "lambda function assume role policy"
  default     = ""
}

variable "role_policy" {
  type        = string
  description = "lambda function role policy"
  default     = ""
}
